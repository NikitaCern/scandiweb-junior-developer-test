<?php
//This file contains most of the functionallity for the website
include 'classes/database.php';

//This is for deleting things;
if (isset($_POST['remove'])){
    $to_deleted = $_POST['checkbox'];

    $db = new database();
    $db->deleteProducts($to_deleted);
    $db->close();

    header('Location: /index.php'); // Redirects home
}
