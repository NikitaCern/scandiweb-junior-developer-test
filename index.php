<?php include "includes/header.php"; ?>
    <title>Product list</title>
  </head>
  <body>
    <div class="wrapper">
      <header>
      <div class="container">
        <div class="row align-items-center">
          <h1 class="col md-auto" >Product List</h1>
          <div class="col col-auto">
            <button type="button" name="add" class="btn btn-primary" onClick="window.location = 'add.php'" >ADD</button>
            <button type="submit" form="remove" name="remove" class="btn btn-secondary">MASS DELETE</button>
          </div>
        </div>
      </div>
      <hr class="container">
    </header>
    <main>
      <div class="container">
          <div class="row">
            <form class="row" action="/master.php" method="post" id="remove">

          <?php
              include 'classes/database.php';

              $db = new database();
              $products = $db->getProductObjectArray();
              $db->close();

              foreach ($products as $product){
                echo '<div class="col col-3 pb-4">
                          <div class="border h-100">
                          <input type="checkbox" name="checkbox[]"  id="checkbox[]" value="' . $product->getId() . '" class="input">
                            <p>' . $product->getSku() . '<br>' . $product->getName() . '<br>' . $product->formatedPrice() . '<br>' . $product->formatedOutput() . '</p>
                            </div>
                          </div>';
              }
              ?>

        </form>
        </div>
      </div>
    </main>

<?php include "includes/footer.php"; ?>
