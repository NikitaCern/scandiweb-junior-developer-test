<?php  include"includes/header.php";

include 'classes/database.php';
//This is for adding things;
if (isset($_POST['save'])){

    $error = [];

    $sku = htmlspecialchars(trim($_POST['sku']));
    $name = htmlspecialchars(trim($_POST['name']));

    if(empty($sku)){
      $error['sku'] = "sku can't be empty! ";
    }else if(!ctype_alnum($sku) || strlen($sku) <3){
      $error['sku'] = "sku must contain only letters and numbers, and be atleast 3 symbols long! ";
    }


    if(empty($name)){
      $error['name'] = "name can't be empty! ";
    }else if(!ctype_alnum($name) || strlen($name) <3){
      $error['name'] = "name must contain only letters and numbers, and be atleast 3 symbols long! ";
    }

    if(empty($error)){
      $product_type = $_POST["type"];
      $product = new $product_type($_POST);
      $db = new database();
      $db->addProduct($product);
      $db->close();
      header('Location: /index.php');
    }
}?>
    <title>Product Add</title>
  </head>
  <body>
    <div class="wrapper">
    <header>
      <div class="container">
        <div class="row align-items-center">
          <h1 class="col md-auto" >Product Add</h1>
          <div class="col col-auto">
            <button type="submit"  form="add"  name="save" class="btn btn-primary">Save</button>
            <button type="button" name="cancel" onClick="window.location = 'index.php'" class="btn btn-secondary" >Cancel</button>
          </div>
        </div>
      </div>
      <hr class="container">
    </header>
    <main>
      <div class="container">
        <form id="add" action="/add.php" method="post" class="row">
          <div class="form-group row">
            <span class="text-danger"><?php echo $error['sku'] ?? ""; ?></span>
            <label for="SKU" class="col col-3 col-form-label">SKU</label>
            <div class="col col-6">
                <input type="text" class="form-control" name="sku" id="sku" placeholder="AAA1234567890" value='<?php echo $sku ?? ""; ?>'>
            </div>
          </div>

          <div class="form-group row">
              <span class="text-danger"><?php echo $error['name'] ?? ""; ?></span>
            <label for="name" class="col col-3 col-form-label">Name</label>
            <div class="col col-6">
              <input type="text" class="form-control" name="name" id="name" placeholder="Product name"value='<?php echo $name ?? ""; ?>'>
            </div>
          </div>

          <div class="form-group row">
            <label for="price" class="col col-3 col-form-label">Price ($)</label>
            <div class="col col-6">
              <input type="number" class="form-control" name="price" value=0.00 min="0.00" step="0.01" id="price" required >
            </div>
          </div>

          <div class="form-group row">
            <label for="type" class="col col-3 col-form-label">Type Switcher</label>
            <div class="col col-6">
              <select class="form-control" name="type" id="productSelect">
                <option value="Dvd" selected>DVD</option>
                <option value="Book">Book</option>
                <option value="Furniture">Furniture</option>
              </select>
            </div>
          </div>

          <hr>

            <script>
              var selector = document.getElementById("productSelect");  //First we get the select element

              selector.onchange =function(){                            //when it changes, we do the following
              var options = document.querySelectorAll("div.info");      //We get all the additional info boxes

                options.forEach(function(option){                       // for each of them we
                  option.style.display = "none";                        // hide them

                  if (option.id == selector.value) {                    // if they are the ones currently selected
                    option.style.display = "block";                     // we show them
                  }

                  var inputs = option.querySelectorAll("input");        //We get all the input fields in those fields
                  inputs.forEach(function(input){                       //And for each of those fields
                    input.value = 0;                                   //Clear their values
                  });
                });
              };
            </script>

            <!--For DVD-->
          <div id="Dvd" class="info">
            <div class="form-group row">
              <label for="size" class="col col-3 col-form-label">Size (MB)</label>
              <div class="col col-6">
                <input type="number" class="form-control" name="size"  min="0" value="0" step="1" id="size">
              </div>
              <small class="form-text text-muted">
                Please provide the size/capacity. Most common sizes are:<br>
                4.7GB (47000 MB) for single-layer, single-sided discs<br>
                8.5GB (85000 MB) for dual-layer, single-sided discs
              </small>
            </div>
          </div>

          <!--For Books-->
          <div id="Book" style="display:none" class="info">
            <div class="form-group row">
              <label for="weight" class="col col-3 col-form-label">Weight (Kg)</label>
              <div class="col col-6">
                <input type="number" min="0.0" step="0.1"  class="form-control" value="0" name="weight" id="weight">
              </div>
              <small class="form-text text-muted">
                Please provide the weight of the book in Kg.<br>
                To convert from pounds, multiply the weight by 0.45Kg
              </small>
            </div>
          </div>

          <!--For furniture -->
          <div id="Furniture" style="display:none" class="info">
            <div class="form-group row">
              <label for="height" class="col col-3 col-form-label">Height (cm)</label>
              <div class="col col-6">
                <input type="number" class="form-control"  min="0"  step="1" name="height" value="0" id="height">
              </div>
            </div>

            <div class="form-group row">
              <label for="width" class="col col-3 col-form-label">Width (cm)</label>
              <div class="col col-6">
                <input type="number" class="form-control"  min="0" step="1"  name="width" value="0" id="width">
              </div>
            </div>

            <div class="form-group row">
              <label for="length" class="col col-3 col-form-label">Length (cm)</label>
              <div class="col col-6">
                <input type="number" class="form-control" min="0" step="1"  name="length" value="0" id="length">
              </div>
            </div>
            <small class="form-text text-muted">
              Please provide dimensions of your item.<br>
              To convert from inches, multiply the distance by 2.54cm
            </small>
          </div>

        </form>
      </div>
    </main>
<?php  include"includes/footer.php"; ?>
