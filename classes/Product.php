<?php
//this file contains the defenition of Product class

include  'classes/Furniture.php';
include  'classes/Book.php';
include  'classes/Dvd.php';

abstract class Product {

  private $id;
  private $sku;
  private $name;
  private $price;

  public function __construct(array $arg){
      $this->id = $arg['id'] ?? 0;
      $this->sku = $arg['sku'];
      $this->name = $arg['name'];
      $this->price = $arg['price'];
  }

  public function setId($id){
    $this->id = $id;
  }
  public function setSku($sku){
    $this->sku = $sku;
  }
  public function setName($name){
    $this->name = $name;
  }
  public function setPrice($price){
     $this->price = $price;
   }

  public function getId(){
     return htmlspecialchars($this->id);
   }

  public function getSku(){
     return htmlspecialchars($this->sku);
   }

  public function getName(){
     return htmlspecialchars($this->name);
   }

  public function getPrice(){
     return htmlspecialchars($this->price);
   }

  protected function returnSql(){
      $sql = "('" . $this->sku . "', '" . $this->name . "', " . $this->price. ", ";
      return $sql;
  }

  public function formatedPrice(){
    return $this->price . " $";
  }
}
