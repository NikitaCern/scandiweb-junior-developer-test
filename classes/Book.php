<?php

//this file contains the defenition of Book class

class Book extends Product{
  private $weight;

  public function __construct(array $arg){
    parent::__construct($arg);
    $this->weight = $arg['weight'];
  }

  public function getWeight(){
    return $this->weight;
  }

  public function setWeight($weight){
    $this->weight = htmlspecialchars($weight);
  }

  public function returnSql(){
      $sql = "INSERT INTO products (sku, name, price, productType, weight ) VALUES ";
      $sql .= parent::returnSql();
      $sql .= "'Book', " . $this->weight . ");";
      return $sql;
  }

  public function formatedOutput(){
    return "Weight: "  . $this->weight . "KG";
  }
}
