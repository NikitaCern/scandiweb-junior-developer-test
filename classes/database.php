<?php
// This file is for the connection settings to the database

include  'classes/Product.php';

class database{

  protected $conn;

  function __construct($host = "localhost", $username = "root", $password = "",  $dbname = "scandiweb"){
    $this->conn = mysqli_connect($host, $username, $password, $dbname);
    if (!$this->conn) {
      echo 'Connection error!' . $this->conn;
    }
  }

  function close(){
    if (!$this->conn) {
      $this->conn -> close();
    }
  }

  function getProductObjectArray(){
    $array = array();

    if (!$this->conn) {
      echo 'Connection error!' . $this->conn;
      return $array;
    }

    $sql='SELECT * FROM products';

    $query = mysqli_query($this->conn, $sql);

    $result = mysqli_fetch_all($query, MYSQLI_ASSOC);

    foreach($result as $object){
      $product = new $object["productType"]($object);
      array_push($array, $product);
    }

    return $array;
  }

  function deleteProducts($array){
    if (!$this->conn) {
      echo 'Connection error!' . $this->conn;
      return;
    }

    foreach ($array as $object) {
      $sql = "DELETE FROM products WHERE id= " . $object;
      $this->conn->query($sql);
    }
    return;
  }

  function addProduct($product){
    if (!$this->conn) {
      echo 'Connection error!' . $this->conn;
      return;
    }

    $sql = $product->returnSql();

    $this->conn->query($sql);
    return;
  }
}
