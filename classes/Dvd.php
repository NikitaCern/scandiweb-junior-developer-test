<?php

//this file contains the defenition of Dvd class

class Dvd extends Product {
  private $size;

  public function __construct(array $arg){
    parent::__construct($arg);
    $this->size = $arg['size'];
  }

  public function getSize(){
    return $this->size;
  }

  public function setSize($size){
    $this->size = htmlspecialchars($size);
  }

  public function returnSql(){
      $sql =  "INSERT INTO products (sku, name, price, productType, size ) VALUES ";
      $sql .= parent::returnSql();
      $sql .= "'Dvd', " . $this->size . ");";
      return $sql;
  }

  public function formatedOutput(){
    return "Size: "  . $this->size . " MB";
  }
}
