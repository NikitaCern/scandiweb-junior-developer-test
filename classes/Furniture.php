<?php

//this file contains the defenition of Furniture class

class Furniture extends Product{
  private $height;
  private $width;
  private $length;

  public function __construct(array $arg){
    parent::__construct($arg);
    $this->height = $arg['height'];
    $this->width = $arg['width'];
    $this->length = $arg['length'];
  }

  public function getHeight(){
    return $this->height;
  }
  public function getWidth(){
    return $this->width;
  }
  public function getLength(){
    return $this->length;
  }

  public function setHeight($height){
    $this->height = htmlspecialchars($height);
  }
  public function setWidth($width){
    $this->width = htmlspecialchars($width);
  }
  public function setLength($length){
    $this->length = htmlspecialchars($length);
  }

  public function returnSql(){
      $sql = "INSERT INTO products (sku, name, price, productType, height, length, width ) VALUES ";
      $sql .= parent::returnSql();
      $sql .= "'Furniture', " . $this->height . ", " . $this->length . ", ". $this->width. ");";
      return $sql;
  }

  public function formatedOutput(){
    return "Dimensions: "  . $this->height . "x" . $this->width . "x" . $this->length;
  }
}
